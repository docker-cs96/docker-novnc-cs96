FROM ubuntu:18.04

ENV DEBIAN_FRONTEND noninteractive
ENV HOME /root

############ core novnc install ###############
RUN apt-get update && apt-get -yq dist-upgrade \
 && apt-get install -yq --no-install-recommends \
    wget \
    bzip2 \
    ca-certificates \
    apt-utils

RUN apt-get update   &&  apt-get dist-upgrade -y

RUN apt-get install -y --force-yes --no-install-recommends \
        python-numpy \
        software-properties-common \
        wget \
        curl \
        supervisor \
        openssh-server \
        pwgen \
        sudo \
        iputils-ping \
        vim-tiny \
        net-tools \
        lxde \
        lxde-common \
        menu \
        openbox \
        openbox-menu \
        xterm \
        obconf \
        obmenu \
        xfce4-terminal \
        python-xdg \
        scrot \
        x11vnc \
        xvfb \
        gtk2-engines-murrine \
        ttf-ubuntu-font-family \
        firefox \
        firefox-dev \
        libwebkitgtk-3.0-0 \
        xserver-xorg-video-dummy \
        xdg-utils \
    && apt-get autoclean \
    && apt-get autoremove \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir /etc/startup.aux/
RUN echo "#Dummy" > /etc/startup.aux/00.sh
RUN chmod +x /etc/startup.aux/00.sh
RUN mkdir -p /etc/supervisor/conf.d
RUN rm /etc/supervisor/supervisord.conf

# create an ubuntu user who cannot sudo
RUN useradd --create-home --shell /bin/bash --user-group ubuntu
RUN echo "ubuntu:badpassword" | chpasswd

ADD startup.sh /
ADD cleanup-cruft.sh /
ADD initialize.sh /

ADD supervisord.conf.xorg /etc/supervisor/supervisord.conf
EXPOSE 6080

ADD openbox-config /openbox-config
RUN cp -r /openbox-config/.config ~ubuntu/
RUN chown -R ubuntu ~ubuntu/.config ; chgrp -R ubuntu ~ubuntu/.config
RUN rm -r /openbox-config

# noVNC
ADD noVNC /noVNC/

# make sure the noVNC self.pem cert file is only readable by root
RUN   chmod 400 /noVNC/self.pem

# store a password for the VNC service
RUN mkdir /home/root
RUN mkdir /home/root/.vnc
RUN x11vnc -storepasswd foobar /home/root/.vnc/passwd
ADD xorg.conf /etc/X11/xorg.conf

############ end core novnc install ###############



############ begin helpful packages ###############
RUN apt-get update && apt-get install -yq \
 build-essential \
 libpng-dev \
 zlib1g-dev \
 libjpeg-dev \
 zip \
 unzip \
 git 
############ end helpful packages ###############



############ begin visual studio ###############
WORKDIR /opt
RUN apt-get install -y libnotify4 libnss3
RUN wget -O visual_studio.deb https://go.microsoft.com/fwlink/\?LinkID\=760868
RUN dpkg -i visual_studio.deb
# ############ end visual studio ###############


############ begin jdk-17  ###############
#
RUN wget https://download.oracle.com/java/17/latest/jdk-17_linux-x64_bin.deb
RUN apt-get install -y ./jdk-17_linux-x64_bin.deb
RUN rm jdk-17_linux-x64_bin.deb
#
############ end jdk-17 ###############


# ############ begin alice 3  ###############
#RUN wget https://www.alice.org/wp-content/uploads/2019/04/Alice3_unix_3_5.sh
#RUN DEBIAN_FRONTEND=noninteractive bash Alice3_unix_3_5.sh -q
RUN wget https://github.com/TheAliceProject/alice3/releases/download/3.7.0.0/alice3_linux_bundle_3_7_0_0+build_876.deb
RUN DEBIAN_FRONTEND=noninteractive apt-get install ./alice3_linux_bundle_3_7_0_0+build_876.deb
RUN rm alice3_linux_bundle_3_7_0_0+build_876.deb

#fix "sim" render
RUN apt-get install -y libglu1 
WORKDIR /
# ############ end alice 3 ###############


# get rid of some LXDE & OpenBox cruft that doesn't work and clutters menus
RUN rm /usr/share/applications/display-im6.q16.desktop & \
    rm /usr/share/applications/display-im6.desktop & \
    rm /usr/share/applications/lxterminal.desktop & \
    rm /usr/share/applications/debian-uxterm.desktop & \
    rm /usr/share/applications/x11vnc.desktop & \
    rm /usr/share/applications/lxde-x-www-browser.desktop & \
    ln -s /usr/share/applications/firefox.desktop /usr/share/applications/lxde-x-www-browser.desktop & \
    rm /usr/share/applications/lxde-x-terminal-emulator.desktop  & \
    rm -rf /usr/share/ImageMagick-6


#https://localhost:6080/vnc.html?host=localhost&port=6080

ENTRYPOINT ["/startup.sh"]
